# <Router>

A simple classs to manage user queries.

## Download

```
$ git clone git@bitbucket.org:likerRr/router.git
```

## First Run

Example project can catch one of two user queries:

* `http://your.host/`
* `http://your.host/article/2/hello%20world/24.11.2013`

## .htaccess
Rewrite your `.htaccess` file to redirect all user queries to `index.php` file:
```
#!htaccess

RewriteEngine On
RewriteBase /
RewriteCond %{REQUEST_FILENAME} !-f
RewriteCond %{REQUEST_FILENAME} !-d
RewriteRule ^(.*)\?*$ index.php/$1 [L,QSA]
```
## Configuration
To add new route, use:
```
#!php

Routes::add($name, $url, $parameters);
```
It has 3 parameters:

* route name. Using to identificate route. Required
* route address with parameters. Used to determine URL. Required
* array of parameters. Used to determine URL parameters. Not required

To attach a file to the route, call method `file($name)` from Routes object, like:
```
#!php

Routes::add('main', '')->file('main');
```
Attached file will be searched in document root folder. To change included files folder, use static method `setAppPath($path)`. That path will be combined with project root folder:
```
#!php

// now, attached files will be search in "DOCUMENT_ROOT/application/" folder
Routes::setAppPath('application/'); 
```
Now you have all to see <Router> in work. To detect route and load file, just call static method `detect()`:
```
#!php

echo Routes::detect();
```
## Parameters
You can configure Router url parameters. Any parameter must be written like an `HTML tag`, in angle brackets. To designate uri as not required use parenthesis. E.g.:
```
#!php

Routes::add('articles', 'article/<id>(/<name>)')
```
In this example, `<id>` parameter must be specify in URL, but `<name>` is can be or not to be. There are some example URLs for these route:
```
#!php

// not correct, cause id is required
http://your.host/article/
```
```
#!php

// correct, cause <id> is specified and <name> is not required
http://your.host/article/2
```
```
#!php

// correct, cause <id> and <name> are specified
http://your.host/article/2/hello%20world
```
Any of parameters must be described with regExp, e.g.:
```
#!php

Routes::add('articles', 'article/<id>(/<name>)', array(
	'id' => '\d+',
	'name' => '[a-z\s]+'
));
```
In that example `<id>` must be only digits, `<name>` can be alphabetical with spaces.

To have access to parameters, use static method `param($name)`:
```
#!php

Routes::param('id');
```
## Security
To defend your scripts from internal access, define `DOCROOT` and `ANTIHACK` constants in the top of your `index.php` file. Then check if it defined in all single file included from `index.php`:
```php
// index.php
define('DOCROOT', $_SERVER['DOCUMENT_ROOT'] . '/');
define('APPPATH', 'application' . '/');

// applicaton/article.php
<?php defined('DOCROOT') or die(ANTIHACK);
..
```
Also defend you folders with `.htaccess`:
```
#!.htaccess

RewriteRule ^(?:application|classes|includes|assets)\b.* index.php/$0 [L]
```
## Exceptions
To catch Route exception, use Route_Exception class. To show debug info, use debug($isProduction) method:
```
#!php

try {
	echo Routes::detect();
}
catch (Route_Exception $e) {
	echo $e->debug(false);
}
```

## Have fun with <Router>!