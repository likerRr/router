<?php defined('DOCROOT') or die(ANTIHACK);
	/**
	 * Auto Load Classes
	 * @param $class
	 */
	function __autoload($class) {
		include DOCROOT . 'classes/' . $class . '.php';
	}