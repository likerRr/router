<?php defined('DOCROOT') or die(ANTIHACK);

	Routes::setAppPath(APPPATH);
	/**
	 * Articles
	 */
	Routes::add('articles', 'article/<id>((/<name>)(/<date>)|(/<date>)(/<name>))', array('id' => '\d+', 'name' => '[a-z\s]+', 'date' => '\d{2}(/|\.)\d{2}(/|\.)\d{2,4}'))
		->file('article');
	/**
	 * Main Page
	 */
	Routes::add('main', '')
		->file('main');