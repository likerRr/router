<?php
//	define('DOCROOT', $_SERVER['DOCUMENT_ROOT'] . '/');
	define('DOCROOT', __DIR__ . '/');
	define('APPPATH', 'application' . '/');
	define('ANTIHACK', 'Hm, something going wrong. Try again from the beginning');

	include_once DOCROOT . 'includes/functions.php';
	include_once DOCROOT . 'includes/routes.php';

	try {
		echo Routes::detect();
	}
	catch (Route_Exception $e) {
		echo $e->debug(false);
	}